describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});

describe('Even Test', function() {
    describe('isEven()', function() {
      it('should return true', function() {
        assert.isOk(isEven(2));
      });
      it('should return false', function() {
        assert.isNotOk(isEven(1));
      });
      it('should return true on negative even test', function() {
        assert.isOk(isEven(-2));
      });
      it('should return false on negative odd test', function() {
        assert.isNotOk(isEven(-1));
      });
      it('should return false if a char is given', function() {
        assert.isNotOk(isEven("e"));
      });
      it('should return false if a symbol is given', function() {
        assert.isNotOk(isEven("*"));
      });
    });
  });
  
describe('Bonjour Test', function() {
  describe('direBonjour()', function() {
    it('should say hello', function() {
      expect(bonjour("John")).to.equal("Bonjour, John")
    });
  });
});

  
describe('NumSecu Test', function() {
  describe('checkNumSecu()', function() {
    it('should not pass with empty number', function() {
      assert.isNotOk(checkNumSecu("","aa"));
    });
    it('should not pass with empty key', function() {
      assert.isNotOk(checkNumSecu("aa",""));
    });
  });
});

describe('Fibonacci Test', function() {
  describe('fibonacci()', function() {
    it('fibonacci(1)', function() {
      expect(fibonacci(1)).to.equal(1);
    });
    it('fibonacci(2)', function() {
      expect(fibonacci(2)).to.equal(1);
    });
    it('fibonacci(15)', function() {
      expect(fibonacci(15)).to.equal(610);
    });
  });
});

describe('Rib Test', function() {
  describe('test isRIBvalid()', function() {
    it('should validate a rib', function() {
      assert.isOk(isRIBvalid("30001","00794","12345678901"))
    });
  });
});

describe('Mail Test', function() {
  describe('validateEmail()', function() {
    it('no @', function() {
      assert.isNotOk(validateEmail("test"))
    });
    it('no dot after @', function() {
      assert.isNotOk(validateEmail("test@test"))
    });
    it('nothing before @', function() {
      assert.isNotOk(validateEmail("@test.com"))
    });
    it('1 letter domain name', function() {
      assert.isNotOk(validateEmail("test@example.c"))
    });
    it('valid', function() {
      assert.isOk(validateEmail("test@example.com"))
    });
  });
});
