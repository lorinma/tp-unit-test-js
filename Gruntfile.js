module.exports = function(grunt) {
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      concat: {
        options: {
          // define a string to put between each file in the concatenated output
          separator: ';'
        },
        dist: {
          // the files to concatenate
          src: ['js/*.js'],
          // the location of the resulting JS file
          dest: 'dist/<%= pkg.name %>.js'
        },
      },
      uglify: {
        targetdist: {
            src: ['<%= concat.dist.dest %>'],
            dest: 'dist/<%= pkg.name %>.min.js'
        },

       },
       watch: {
            scripts: {
              files: ['js/*.js','tests/tests.js'],
              tasks: ['concat', 'uglify','mocha'],
              options: {
                livereload: 9001
              },
            },
       },
       mocha: {
        test: {
          options: {
            reporter: 'XUnit'
          },
          src: ['html/test.html'],
          dest: 'log/xunit.xml'
        },
      }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-mocha');

    grunt.registerTask('default', ['concat', 'uglify','mocha']);
};